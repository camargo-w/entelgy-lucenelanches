package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.TypeOfBread;
import org.springframework.data.repository.CrudRepository;

public interface TypeOfBreadRepository extends CrudRepository<TypeOfBread, Long>{

}
