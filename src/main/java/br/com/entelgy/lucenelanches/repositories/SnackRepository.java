package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Snack;
import br.com.entelgy.lucenelanches.models.TypeOfSnack;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SnackRepository extends CrudRepository<Snack, Long>{

	List<Snack> findByType(TypeOfSnack type);	
	
}
