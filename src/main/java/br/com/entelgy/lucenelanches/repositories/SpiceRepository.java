package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Spice;
import org.springframework.data.repository.CrudRepository;

public interface SpiceRepository extends CrudRepository<Spice, Long>{

}
