package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Cheese;
import org.springframework.data.repository.CrudRepository;

public interface CheeseRepository extends CrudRepository<Cheese, Long>{

}
