package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Salad;
import org.springframework.data.repository.CrudRepository;

public interface SaladRepository extends CrudRepository<Salad, Long>{

}
