package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Filling;
import org.springframework.data.repository.CrudRepository;

public interface FillingRepository extends CrudRepository<Filling, Long> {

}
