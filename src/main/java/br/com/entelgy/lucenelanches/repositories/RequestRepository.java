package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Request;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request, Long>{

}
