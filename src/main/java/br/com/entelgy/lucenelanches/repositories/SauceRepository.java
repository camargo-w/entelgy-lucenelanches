package br.com.entelgy.lucenelanches.repositories;

import br.com.entelgy.lucenelanches.models.Sauce;
import org.springframework.data.repository.CrudRepository;

public interface SauceRepository extends CrudRepository<Sauce, Long>{

}
