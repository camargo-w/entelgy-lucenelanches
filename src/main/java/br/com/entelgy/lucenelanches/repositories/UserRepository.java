package br.com.entelgy.lucenelanches.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.entelgy.lucenelanches.models.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    User findByUsername(String username);
}
