package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Spice;
import br.com.entelgy.lucenelanches.repositories.SpiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpiceService {

    @Autowired
    private SpiceRepository spiceRepository;

    public Iterable<Spice> findAll() {
        return spiceRepository.findAll();
    }

}
