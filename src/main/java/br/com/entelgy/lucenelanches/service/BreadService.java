package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.TypeOfBread;
import br.com.entelgy.lucenelanches.repositories.TypeOfBreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BreadService {

    @Autowired
    private TypeOfBreadRepository typeOfBreadRepository;

    public Iterable<TypeOfBread> findAll() {
        return typeOfBreadRepository.findAll();
    }
}
