package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Sauce;
import br.com.entelgy.lucenelanches.repositories.SauceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SauceService {

    @Autowired
    private SauceRepository sauceRepository;

    public Iterable<Sauce> findAll(){
        return sauceRepository.findAll();
    }

}
