package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Snack;
import br.com.entelgy.lucenelanches.models.TypeOfSnack;
import br.com.entelgy.lucenelanches.repositories.SnackRepository;
import br.com.entelgy.lucenelanches.repositories.SnackTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnackService {

    @Autowired
    private SnackRepository snackRepository;

    @Autowired
    private SnackTypeRepository snackTypeRepository;

    public Snack byId(Long id) {
        return this.snackRepository.findOne(id);
    }

    public Iterable<TypeOfSnack> findAll(){
        return snackTypeRepository.findAll();
    }
}
