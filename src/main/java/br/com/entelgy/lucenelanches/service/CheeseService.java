package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Cheese;
import br.com.entelgy.lucenelanches.repositories.CheeseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheeseService {

    @Autowired
    private CheeseRepository cheeseRepository;

    public Iterable<Cheese> findAllCheeses(){
        return cheeseRepository.findAll();
    }

}
