package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Filling;
import br.com.entelgy.lucenelanches.repositories.FillingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FillingService {

    @Autowired
    private FillingRepository fillingRepository;

    public Iterable<Filling> findAll() {
        return this.fillingRepository.findAll();
    }

}
