package br.com.entelgy.lucenelanches.service;

import br.com.entelgy.lucenelanches.models.Salad;
import br.com.entelgy.lucenelanches.repositories.SaladRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaladService {

    @Autowired
    private SaladRepository saladRepository;

    public Iterable<Salad> findAll(){
        return this.saladRepository.findAll();
    }

}
