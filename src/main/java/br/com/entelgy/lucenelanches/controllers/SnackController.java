package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Snack;
import br.com.entelgy.lucenelanches.models.TypeOfSnack;
import br.com.entelgy.lucenelanches.repositories.SnackRepository;
import br.com.entelgy.lucenelanches.repositories.SnackTypeRepository;
import br.com.entelgy.lucenelanches.service.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/snacks")
public class SnackController {

    @Autowired
    private SnackRepository snackRepository;

    @Autowired
    private SnackTypeRepository snackTypeRepository;

    @Autowired
    private SnackService service;

    @GetMapping("/types")
    public @ResponseBody Iterable<TypeOfSnack> listTypes() {
//        return service.listTypes("lanches");
        return service.findAll();
    }

    @GetMapping("/byId")
    public @ResponseBody Snack byId(Long id) {
        return service.byId(id);
    }

}
