package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Filling;
import br.com.entelgy.lucenelanches.service.FillingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/fillings")
public class FillingController {

    @Autowired
    private FillingService service;

    @GetMapping
    public @ResponseBody Iterable<Filling> findAll() {
        return service.findAll();
    }

}
