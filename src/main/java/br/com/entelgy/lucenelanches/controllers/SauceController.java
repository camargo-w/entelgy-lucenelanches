package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Sauce;
import br.com.entelgy.lucenelanches.service.SauceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sauces")
public class SauceController {

	@Autowired
	private SauceService service;

	@GetMapping
	public @ResponseBody Iterable<Sauce> findAll() {
		return service.findAll();
	}

}
