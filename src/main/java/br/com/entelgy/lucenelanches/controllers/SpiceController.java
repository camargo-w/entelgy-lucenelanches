package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Spice;
import br.com.entelgy.lucenelanches.service.SpiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/spices")
public class SpiceController {

    @Autowired
    private SpiceService service;

    @GetMapping
    public @ResponseBody Iterable<Spice> findAll() {
        return service.findAll();
    }

}
