package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.TypeOfBread;
import br.com.entelgy.lucenelanches.service.BreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/breads")
public class BreadController {
	
	@Autowired
	private BreadService service;
	
	@GetMapping
	public @ResponseBody Iterable<TypeOfBread> findAll() {
		return service.findAll();
  }

}
