package br.com.entelgy.lucenelanches.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String login(Authentication auth){

        if (auth!=null && auth.isAuthenticated()){
            return "redirect:/orders";
        }
        return "login";
    }
}
