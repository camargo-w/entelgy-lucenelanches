package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Cheese;
import br.com.entelgy.lucenelanches.service.CheeseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CheeseController {

    @Autowired
    private CheeseService service;

    @RequestMapping("/cheeses")
    public @ResponseBody
    Iterable<Cheese> findAll() {
        return service.findAllCheeses();
    }

}
