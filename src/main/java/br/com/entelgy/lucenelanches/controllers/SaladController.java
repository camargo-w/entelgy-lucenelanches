package br.com.entelgy.lucenelanches.controllers;

import br.com.entelgy.lucenelanches.models.Salad;
import br.com.entelgy.lucenelanches.service.SaladService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/salads")
public class SaladController {

	@Autowired
	private SaladService service;

	@GetMapping
	public @ResponseBody Iterable<Salad> findAll() {
		return service.findAll();
	}

}
