# Instruções de instalação

Para baixar e rodar o projeto é necessário fazer clone do respositório no bitbucket:
URL: `git clone https://camargo-w@bitbucket.org/camargo-w/entelgy-lucenelanches.git`

* Caso não tenha o Git instalado na máquina, rode o seguinte comando no terminal:
`sudo apt-get install git`

Após realizado o clone do repositório, acesse o diretório do projeto onde realizou o clone e rode o seguinte comando no terminal:
`mvn spring-boot:run`

* Caso não tenha o Maven instalado na máquina, rode o seguinte comando no terminal:
`sudo apt-get install maven`


O projeto já está com uma carga de banco de dados pré populada para fins de teste.
* Banco de dados: MySQL
* Schema: lucene_lanches
* Usuário: root
* Senha: root

* Usuário da aplicação para testes:

``user: wellington senha: teste``

# Decisões tomadas para o desenvolvimento do projeto

Busquei utilizar somente tecnologias opensource no desenvolvimento da aplicação.

## MySQL
Banco de dados relacional, opensource e muito utilizado em diversas aplicações pelo mundo.

## Spring Boot
Essa tecnologia busca agilizar o desenvolvimento de aplicações Java, fornecendo uma gama imensa de recursos para facilitar o desenvolvimento.
Framework gratuito, robusto e muito utilizado.

### Spring MVC
Oferece suporte MVC para aplicações Spring.

### Spring Data JPA
Oferece suporte ORM para facilitar operações com banco de dados

### Spring Security
Oferece suporte a segurança da aplicação, como autenticação, prevenção de tipos de ataques, sanitização de informação.

### Spring Thymeleaf
Oferece suporte para trabalhar com templates web, facilitando o desenvolvimento de telas para o projeto.

## Mockito
Para auxiliar no desenvolvimento de testes

## Maven
Para gerenciamento de dependências do projeto

## Jquery
Para facilitar o desenvolvimento de código Javascript

## Materialize CSS
Para facilitar as definições de layout do projeto